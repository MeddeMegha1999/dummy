import json
import logging
import pandas as pd
from fastapi import FastAPI, HTTPException
from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

app = FastAPI()

@app.get("/check_string")
def check_string(string: str):
    # Load qualifiers.json
    with open("qualifiers.json", "r") as file:
        qualifer = json.load(file)
    
    # Log the loaded JSON data
    logger.debug(f"Loaded qualifiers.json: {qualifer}")
    
    # Create DataFrame and filter labels
    pd_df = pd.DataFrame(qualifer.get("partDescriptions"))
    logger.debug(f"Created DataFrame: {pd_df}")
    
    search_string = pd_df[pd_df["label"].apply(lambda x: False if "," in x else True)]["label"].to_list()
    search_string_lower = [label.lower() for label in search_string]
    logger.debug(f"Filtered search strings: {search_string_lower}")
    
    if string.lower() in search_string_lower:
        # Setup Selenium WebDriver
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        roads = webdriver.Chrome(options=options)

        try:
            # Access login page
            login_url = "https://roads.quanterion.com/demo/login?u=meddemeghachandra@gmail.com"
            roads.get(login_url)
            logger.debug(f"Accessed login URL: {login_url}")
            sleep(2)
            
            # Access API URL
            api_url = f"https://roads-api.quanterion.com/api-v1/r/summary?b=nprd_2023&b=eprd_2024&b=fmd_2016&descript={string.replace(' ', '%20')}"
            roads.get(api_url)
            logger.debug(f"Accessed API URL: {api_url}")
            sleep(2)
            
            response = roads.find_element(by=By.TAG_NAME, value="body").text
            logger.debug(f"Received response: {response}")
            
            data = json.loads(response)
            roads.quit()
            logger.debug(f"Parsed data: {data}")
            return data["data"]
        except Exception as e:
            roads.quit()
            logger.error(f"An error occurred: {str(e)}")
            raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")
    else:
        logger.debug("String not found in search strings.")
        return {"message": "False"}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
